clear all;
close all;


% Importing the data
dataLog10 = importdata('data.csv', ';')

%Transform Log10 format into linear (b=10^(Log10(a)))
data = 10.^dataLog10

noSubject = size(data,1);
dataP.times = (1:size(data,2))

% Color array for images
colors = repmat('krgbmc',1,300);


%iteration over the subjects (6)
for i = 1%:noSubject
    
   dataP.tcid = data(i,:)
   
   %figure(1)
   
   %clf
   sol = run_model(dataP)
   
   % printing in log10 scale
   %semilogy(dataP.times,dataP.tcid,'Marker','o','LineStyle','none','MarkerFaceColor','green')
   %hold on
   %semilogy(sol.x, sol.y(4,:))
   
end