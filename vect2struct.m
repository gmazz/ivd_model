function par = vect2struct(x, par)
%STRUC2VECT this function take a struct and return a filled vector (with the structure elements)
    
    f = fields(par);
    for i=1:length(f)
        par.(f{i}) = x(i);
    end
    
end

