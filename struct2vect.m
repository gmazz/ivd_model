function x = struct2vect(par)
%STRUC2VECT this function take a struct and return a filled vector (with the structure elements)

    f = fields(par);
    x = zeros(size(f));
    for i=1:length(f)
        x(i) = par.(f{i});
    end
    
end


