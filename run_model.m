function sol = run_model(dataP)

    %params
    T = 10

    % Initial values for %TC, %I1, %I2, V and F respectively
    init(1) = 4 * 10^8                
    init(2) = 0 %4.3 * 10^-2      
    init(3) = 0
    init(4) = 7.5 * 10^-2  
    init(5) = 1 %(ng/ml)

    params.b = 3.2*10^-5 
    params.d = 5.2
    params.c = 5.2
    params.p_i = 4.6*10^-2
    params.k_i = 4.0
    
    % IFN parameters
    params.s = 1
    params.a = 1 
    params.tau = 0.5
    
    %Epsilon establish the effect of IFN have on either:
    %1. The movement of infected cells in the virion producing stage k
    %2. The rate of viral production p
    params.e1 = 1*10^-9
    params.e2 = 1*10^-6
    
    [parNew, err] = dataFit(@IVD_model, params, dataP)
    %sol = IVD_model(T, init, parNew)

end

