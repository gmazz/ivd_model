function sol = IVD_model (T, init, params)
    
    sol = ode45(@model,[0,T(1:end)], init, params);
    sol = deval(sol, T(1:end)); % this function take all the solution sol and map it on the Time scale T
    
    function y = model(t, x)
      
      %Tc = x(1)
      %I1 = x(2)
      %I2 = x(3)
      %V = x(4)
      %F = x(5)
      
      y = zeros(5,1);

      k = params.k_i/(1 + params.e1 * x(5)) % not very influencing
      p = params.p_i/(1 + params.e2 * x(5))
      
      y(1) = -params.b * x(1) * x(4);
      y(2) = (params.b * x(1) * x(4)) - (k * x(2));
      y(3) = (k * x(2)) - (params.d * x(3));
      y(4) = (p * x(3)) - params.c * x(4);
      y(5) = (params.s * x(3) * (t-params.tau)) - (params.a * x(5));


      
      
    end
end
