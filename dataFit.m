function [par, err] = dataFit(IVD_model, params, dataP)
     
    vect = struct2vect(params)
    
    %[x, err] = fminsearch(@F, vect);
    %[x, err] = lsqnonlin(@F, vect, zeros(size(vect))); %should give also selected params list 
    [x, err] = lsqnonlin(@F, vect); 
    par = vect2struct(x, params);
    
    function y = F(x)
       parN = vect2struct(x, params);
       y=(feval(IVD_model, dataP.tcid, dataP.times, parN)- dataP.tcid);
    end

end